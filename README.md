# Spring Boot Thymeleaf AJAX Example

## Things to do to run this application
### Clone the repository
```
git clone https://gitlab.com/hendisantika/spring-boot-thymeleaf-ajax-example.git
```

### Go to the folder
```
cd spring-boot-thymeleaf-ajax-example
```

### Run the application
```
mvn clean spring-boot:run
```

## Screen shot

Login Home Page

![Login Home](img/login1.png "Login Home") 

Error Login Home Page

![Error Login Home Page](img/login2.png "Error Login Home") 

Success Login Home Page

![Success  Login Home](img/login3.png "Login Home") 