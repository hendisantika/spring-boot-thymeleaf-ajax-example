package com.hendisantika.springbootthymeleafajaxexample.service;

import com.hendisantika.springbootthymeleafajaxexample.model.LoginForm;
import com.hendisantika.springbootthymeleafajaxexample.model.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-thymeleaf-ajax-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-11
 * Time: 13:05
 */
@Service
public class UserService {

    private List<User> users;

    public List<User> login(LoginForm loginForm) {

        //do stuffs
        //dump user data
        User user = new User(loginForm.getUsername(), loginForm.getPassword(), "uzumaki_naruto@konohagakure.com");

        return new ArrayList<User>(Arrays.asList(user));

    }

}