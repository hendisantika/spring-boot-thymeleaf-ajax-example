package com.hendisantika.springbootthymeleafajaxexample.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-thymeleaf-ajax-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-11
 * Time: 13:06
 */
@Controller
public class MyController {

    private final Logger logger = LoggerFactory.getLogger(MyController.class);

    @GetMapping("/")
    public String index() {
        return "login";
    }

}
